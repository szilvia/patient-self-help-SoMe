# Mapping the content structure of patient self-help group activity on social media

## Aims
We aim to explore the health-related needs of specific patient populations via mapping the content and structure of information flow within patient self-help group activity on social media in order to formulate recommendations regarding the functionality of professional e-health tools for patients living with chronic illnesses in Hungary.

This project constitutes part of a larger investigation entitled “E-patients and e-physicians in Hungary: The role and opportunities of digital health solutions in the healthcare system” conducted as part of a grant awarded by the National Research, Development and Innovation Office, Hungary (Grant: FK_20; Duration: 2020.09-2024.09).
