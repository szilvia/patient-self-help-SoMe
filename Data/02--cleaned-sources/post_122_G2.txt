---
ROCK_attributes:
  -
    groupID: 2
    tid: 122
    year: 2021
    month: 02
---

A Cukorbetegek Egri Egyesületének januári betegoktató foglalkozása 2021. jan.23-án, 14 órától internetes ZOOM ülés keretében lesz. Előadó dr. Domboróczki Zsolt, diabetológus: Együttműködés és motiváció a cukorbetegség kezelése során. Aki szeretne részt venni és lehetősége van internetes csatlakozásra, kérem küldjön egy e-mailt X címre és küldjük a meghívót, amelyre kattintással be tud lépni az előadásra, interaktív beszélgetésre. Minden érdeklődőt szeretettel látunk!
---<<post_delimiter>>---
