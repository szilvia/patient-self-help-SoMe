---
ROCK_attributes:
  -
    groupID: 1
    tid: 4
    year: 2021
    month: 02
---

IGAZ TÖRTÉNET  Egy férfi, aki asztalos munkát végez. Ez az ember minden reggel átmegy egy templom előtt munkába menet. Egy nap meghozta a döntést, hogy be kell mennie a kápolnába, hogy üdvözölje Jézust, mielőtt munkájába indul. Tehát minden nap, amikor elmegy a templom mellett, elmegy a kápolnába, és azt mondja, hogy ′′ Jézus, azért vagyok itt, hogy üdvözöljelek téged sok éven át ezt tette, és része lett. Egy nap a szokásos munkába ment, leesett a tetőről, ahol dolgozott, gerincvelő sérülést szenvedett, sok kórházba vitték, és nem volt orvosság az egészségére, lebénult. Egy nap a kórházban, ahol reménytelenül feküdt betegágyán, felesége magára hagyta a szobában, hogy hozzon valamit a házból. Egy fehérbe öltözött férfi besétált a szobába, és üdvözölte őt, mondván: ′′ Barátom, eljöttem, hogy üdvözöljelek téged. Jézus meglátogatta őt, mivel nem tudott eljönni meglátogatni, mint mindig. Erre a beteg ember megköszönte, elment a látogató, és ez a férfi elkezdte észrevenni a testén a változásokat, és legnagyobb meglepetésére felállt és leült az ágyra, és azt gondolta, hogy álom volt-e, amikor a felesége jött és látta Leült, azt hitte, hogy szellem volt, és riasztást keltett, kiszaladt a szobából. A doktor és nővérek siettek a szobába, azt gondolva, hogy a beteg ember meghalt, amikor a doktor először lépett be, és meglátta a férfit kényelmesen ülve, ő is visszaszaladt, mielőtt eldöntötte, hogy megkérdezi tőle, mi történt, és a gyógyult férfi elmondta neki, hogy mi történt, Sok éven át naponta látogatta Jézust, de Jézus egyetlen látogatása reménytelenségét örömre változtatta! Testvérem, nővérem és barátom, aki ezt az üzenetet olvassa, ez egy igazi élet tanúsága. Valahol ez történt.! Jézus él, ne hagyd, hogy bárki is becsapjon, kérlek. Ahogy beírod az Ámen-t és megosztod ezt az üzenetet 5 különböző Facebook csoportnak Az ősi napok, meglátogat és megváltoztatja a helyzeted 3 nap múlva. 8 év Sok szerencsét annak, aki megosztja 5 csoportba 📷
---<<post_delimiter>>---
media2.giphy.commedia2.giphy.com3
Amen
Amen
Mit keres ez itt ?
