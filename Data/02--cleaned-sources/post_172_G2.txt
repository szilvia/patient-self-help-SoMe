---
ROCK_attributes:
  -
    groupID: 2
    tid: 172
    year: 2020
    month: 08
---

Kellemes ébredés minden koránkelőnek! Most csatlakoztam. Egy kérdésem lenne!  Milyen vércukorszint ingadozás az ami még elfogadható? Egy hete voltam vérvételen. Akkor a reggeli érzelem 4.7. volt a 3 hónapos átlag 7.1. Tegnap reggel (ma még nem mértem) 3.8. Hozzáteszem: sem tegnap, sem amikor ennél jóval magasabb volt az érték soha nem éreztem rosszul magam. Egyszerűen nem is érzem az ingadozást. Köszönöm a segìtségüket!
---<<post_delimiter>>---
Én sajnos igen
