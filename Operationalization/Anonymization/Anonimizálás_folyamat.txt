Instructions for anonymization and cleaning data:

1. Remove all names of people (if the name is an integral part of the text, so it is not only "tagging", then anonymize by replacing it with "XY")
If it is the name of a public figure, you can leave it in the text. If it is the name of a pharmaceutical, company, hospital, location within a city, then do not anonymize.

2. Remove expressions that are inevitably there because of scraping, such as "LikeMore" or "on"

3. Every comment recieves a new line

4. Where the post is a picture with text in it, copy the text from the picture into the source

5. Add the date of the post into the source in the following format:

---
ROCK_attributes:
  -
    groupID: 1
    tid: 1
    year: 2020
    month: 05
---

Leave exactly 4 spaces between the beginning of the line and the word "year" (same with "month")
the picture called "danger" in this directory shows that some programs might "assume" you want a tab at the beginning of a line when you hit enter. be careful! tab does not equal 4 spaces.
in case of months with just one digit, use a zero before the month number.

other rules:
-never add any content to comments
-do not remove links (e.g. gif links)
-if the source is without content after anonymization, that is ok for now
-if the source is empty, do not add the date, just leave it empty (this can happen if the post was a picture and there were no comments under it)
-if the post content was scraped twice by accident (one immediately after the other), remove the second copy.
-if the original post has been removed from the group, write this directly underneath the delimiter: POST REMOVED

the example (picture) in this directory shows an illustration of an anonymized thread. we can check if all comments are on separate lines by using Notepad++ and seeing the line numbers.


--------------------------------------------------------------------------------------------------------
Instrukciók az adatok anonimizálására és megtisztítására:

1. mindenhonnan kivettük a neveket (ha a szöveg SZERVES része a név, tehát nem csak "tag"-elés, hanem pl így: "Róbert Károlynál áttveheti mai nap", akkor így anonimizálunk: "XY-nál áttveheti mai nap")
személyek neve = anonimizáltuk 
ha közéleti személyiség akkor nem anonimizáltuk a nevét
gyógyszerek, cégek, kórházak, közterületek, helységek neve maradt
2. kivettük az olyan kifejezéseket mint "FriLikeMore" - tehát az adatok jellegéből származó plusz, szükségtelen szavakat (és vigyáztunk mert egy csomószor a sor végén van egy felesleges "on" :) )
3. új sorra került minden komment (tehát, egy komment per sor)
4. ahol az eredeti poszt egy kép, de a képben van szöveg, azt beírtuk
5. mindenhol hozzáadtuk a poszt dátumát. ennek formátuma alább (lásd: "year" és "month"):

---
ROCK_attributes:
  -
    groupID: 1
    tid: 1
    year: 2020
    month: 05
---

amikor hozzáadtuk akkor PONTOSAN 4 szóköz volt a sor eleje és a "year" között (ugyanez a month-al).
a "danger" kép a mappában mutatja, hogy a szöveszerkesztő program hajlamos egy "tab"-al reagálni ha enter-t nyomsz a tid: x sor végén (ezt jelzi a nyíl). vigyázzunk, mert a tab nem egyenlő a 4 szóközzel!

az év tekintetében teljes évszám (pl. 2021), a két számjegyű hónapok tekintetében mindkét számjegy, ha egy számjegyű akkor a számot előzze meg egy nulla (02).


egyéb:
-sehol nem adtunk hozzá a képeket (csak a szöveget ami bennük volt, ha volt)
-nem egészítettük ki a thread-eket (pl. új bejegyzésekkel)
-a linkeket, gif-ek linkjét, nem vettük ki
-ha teljesen tartalom-nélküli marad a fájl miután anonimizáltunk, az is ok (per pillanat)
-ha a fájlban eleve nincs tartalom (mert pl kép a poszt és nincs komment), akkor nem raktuk oda az évet és hónapot
-ha a scraping algoritmus valamiért kétszer szedte le a poszt tartalmat és közvetlenül egymás után rakta egy posztba (párszor fordult elő), akkor a duplumot töröltük
-ha a posztot azóta törölték akkor hozzáadtuk a delimiter alá, hogy: POST REMOVED

a példa (kép) itt a mappában jelzi, hogy miként néz ki egy anonimizált thread. ha a Notepad++ progit használjuk, akkor látjuk a sorok számát is (ez hasznos hogy meggyőződjünk arról, egy soron van egy komment)